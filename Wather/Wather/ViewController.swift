//
//  ViewController.swift
//  Wather
//
//  Created by Valiantsin Vasiliavitski1 on 8/3/18.
//  Copyright © 2018 Valiantsin Vasiliavitski1. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class ViewController: UIViewController {
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var weatherMain: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let weather = WeatherGetter()
        weather.view = self
        searchBar.reactive.continuousTextValues.observeValues { (value) in
            if let name = value {
                weather.getWeather(city: name)
            }
        }       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

