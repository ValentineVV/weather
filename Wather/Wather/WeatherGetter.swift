//
//  WeatherGetter.swift
//  Wather
//
//  Created by Valiantsin Vasiliavitski1 on 8/3/18.
//  Copyright © 2018 Valiantsin Vasiliavitski1. All rights reserved.
//

import Foundation
import UIKit

class WeatherGetter {
    
    weak var view: ViewController?
    weak var tableView: TableViewController?
    
    private let openWeatherMapeBaseURL = "http://api.openweathermap.org/data/2.5/find"
    private let openWeatherMapAPIKey = "ca66d774b93a735c91a81e59d29c985b&units=metric&type=like"
    
    func getWeather(city: String) {
        
        let session = URLSession.shared
        
        let weatherRequestURL = URL(string: "\(openWeatherMapeBaseURL)?APPID=\(openWeatherMapAPIKey)&q=\(city)")
        
        weak var weakSelf = self
        
        if let url = weatherRequestURL {
            let dataTask = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
                if let error = error {
                    print("Error:\n\(error)")
                } else {
                    
                    do {
                        let weather = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : AnyObject]
                        if weather["cod"]!.int64Value! != 400 && weather["count"]!.int64Value! > 0 {
                            DispatchQueue.main.sync {
                                print(weather)
                                weakSelf?.tableView?.rowNumber = Int(weather["count"]!.int64Value!)
                                weakSelf?.tableView?.tableView.reloadData()
                            }

                        }
                        
                        
                        //                    print("Date and time: \(weather["dt"]!)")
                        //                    print("City: \(weather["name"]!)")
                        //
                        //                    print("Longitude: \(weather["coord"]!["lon"]!!)")
                        //                    print("Latitude: \(weather["coord"]!["lat"]!!)")
                        //
                        //                    print("Weather ID: \((weather["weather"]![0] as! [String: AnyObject])["id"]!)")
                        //                    print("Weather main: \((weather["weather"]![0] as! [String: AnyObject])["main"]!)")
                        //                    print("Weather description: \((weather["weather"]![0] as! [String: AnyObject])["description"]!)")
                        //                    print("Weather icon ID: \((weather["weather"]![0] as! [String: AnyObject])["icon"]!)")
                        //
                        //                    print("Temperature: \(weather["main"]!["temp"]!!)")
                        //                    print("Humidity: \(weather["main"]!["humidity"]!!)")
                        //                    print("Pressure: \(weather["main"]!["pressure"]!!)")
                        //
                        //                    print("Cloud cover: \(weather["clouds"]!["all"]!!)")
                        //
                        //                    print("Wind direction: \(weather["wind"]!["deg"]!!) degrees")
                        //                    print("Wind speed: \(weather["wind"]!["speed"]!!)")
                        //
                        //                    print("Country: \(weather["sys"]!["country"]!!)")
                        //                    print("Sunrise: \(weather["sys"]!["sunrise"]!!)")
                        //                    print("Sunset: \(weather["sys"]!["sunset"]!!)")
                        //
                        //                    print(weather["name"] as! String)
                        //                    print((weather["weather"]![0] as! [String: AnyObject])["main"] as! String)
                        //                    print(weather["main"]!["temp"]! as! NSNumber)
                        
                        
//                        if (weather["name"] != nil){
//
//                            DispatchQueue.main.async {
//
//                                weakSelf?.view?.cityName.text = weather["name"] as? String
//                                weakSelf?.view?.weatherMain.text = ((weather["weather"] as! [AnyObject])[0] as! [String: AnyObject])["description"] as? String
//                                weakSelf?.view?.temp.text = String.init(describing: (weather["main"]!["temp"]! as! NSNumber)) + "\u{00B0}"
//
//                                let image = ((weather["weather"] as! [AnyObject])[0] as! [String: AnyObject])["icon"] as! String + ".png"
//
//                                let data = try? Data(contentsOf: URL.init(string: "http://openweathermap.org/img/w/" + image)!)
//                                weakSelf?.view?.weatherImage.image = UIImage.init(data: data!)
//
//
//                            }
//                        }
                        
                    } catch let error {
                        print("ERROR:\n\(error)")
                    }
                }
            }
            
            dataTask.resume()
        }
    }
}
