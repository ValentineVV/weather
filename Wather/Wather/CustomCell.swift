//
//  CustomCell.swift
//  Wather
//
//  Created by Valiantsin Vasiliavitski1 on 8/6/18.
//  Copyright © 2018 Valiantsin Vasiliavitski1. All rights reserved.
//

import Foundation
import UIKit

class CustomCell: UITableViewCell {
    
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!    
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var temp: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        city.text = ""
        weatherDescription.text = ""
        temp.text = ""
        weatherImage = nil 
    }
    
}
