//
//  TableViewController.swift
//  Wather
//
//  Created by Valiantsin Vasiliavitski1 on 8/6/18.
//  Copyright © 2018 Valiantsin Vasiliavitski1. All rights reserved.
//

import Foundation
import UIKit
import ReactiveCocoa
import ReactiveSwift

class TableViewController: UITableViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    
    var rowNumber: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib.init(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        let weather = WeatherGetter()
        weak var weakSelf = self
        weather.tableView = self
        searchBar.reactive.continuousTextValues.observeValues { (value) in
            if let name = value {
                weakSelf?.rowNumber = 1
                weather.getWeather(city: name)
                weakSelf?.tableView.reloadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowNumber
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
